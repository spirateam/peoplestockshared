﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeopleStock.Models
{
    public class MainpageModel
    {
        public Person CurrentPerson { get; set; }
        public List<personStatu> Content { get; set; }
        public List<PersonStockTransaction> Transactions { get; set; }
    }

    public class MainOrgPageModel
    {
        public Organization Org { get; set; }
        public List<OrganizationEvent> OrgEvents { get; set; }
        public List<OrganizationContent> AudioContent { get; set; }
        public List<OrganizationContent> VideoContent { get; set; }
        public List<OrganizationStatu> OrgStatus { get; set; }
        public string BannerImgPath { get; set; }
        public string HeaderText { get; set; }
    }
}