﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeopleStock.Models
{
    public class PersonalizeModel
    {
        public string BannerImgLocation { get; set; }
        public string BannerText { get; set; }
    }
}