﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;

namespace PeopleStock.Models
{
    public class Utils
    {

        public static bool IsAudioResource(string fileName)
        {
            bool isAudio = false;
            string ext = Path.GetExtension(fileName).ToLower();
            switch (ext)
            {
                case ".mp3":
                    return true;
            }
            return isAudio;
        }

        public static bool IsVideoResource(string fileName)
        {
            bool isVideo = false;
            string ext = Path.GetExtension(fileName).ToLower();
            switch (ext)
            {
                case ".flv":
                    return true;
            }
            return isVideo;
        }
    }
}