
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 02/28/2013 18:47:07
-- Generated from EDMX file: C:\Dev\Web Projects\PeopleStock\PeopleStock\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PeopleStock];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Organizat__Organ__2B3F6F97]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationStockTransactions] DROP CONSTRAINT [FK__Organizat__Organ__2B3F6F97];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__OrgID__300424B4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationEmployee] DROP CONSTRAINT [FK__Organizat__OrgID__300424B4];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__OrgID__398D8EEE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationContent] DROP CONSTRAINT [FK__Organizat__OrgID__398D8EEE];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__OrgID__46E78A0C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationStatus] DROP CONSTRAINT [FK__Organizat__OrgID__46E78A0C];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__OrgSt__182C9B23]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationStocks] DROP CONSTRAINT [FK__Organizat__OrgSt__182C9B23];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__Owner__1920BF5C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationStocks] DROP CONSTRAINT [FK__Organizat__Owner__1920BF5C];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__Perso__30F848ED]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationEmployee] DROP CONSTRAINT [FK__Organizat__Perso__30F848ED];
GO
IF OBJECT_ID(N'[dbo].[FK__Organizat__Statu__1A14E395]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationStocks] DROP CONSTRAINT [FK__Organizat__Statu__1A14E395];
GO
IF OBJECT_ID(N'[dbo].[FK__personCon__Perso__0F975522]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[personStatus] DROP CONSTRAINT [FK__personCon__Perso__0F975522];
GO
IF OBJECT_ID(N'[dbo].[FK__PersonCon__Perso__4222D4EF]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonContent] DROP CONSTRAINT [FK__PersonCon__Perso__4222D4EF];
GO
IF OBJECT_ID(N'[dbo].[FK__PersonSto__Owner__0519C6AF]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonStocks] DROP CONSTRAINT [FK__PersonSto__Owner__0519C6AF];
GO
IF OBJECT_ID(N'[dbo].[FK__PersonSto__Perso__060DEAE8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonStocks] DROP CONSTRAINT [FK__PersonSto__Perso__060DEAE8];
GO
IF OBJECT_ID(N'[dbo].[FK__PersonSto__Perso__267ABA7A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonStockTransactions] DROP CONSTRAINT [FK__PersonSto__Perso__267ABA7A];
GO
IF OBJECT_ID(N'[dbo].[FK__PersonSto__Statu__0AD2A005]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonStocks] DROP CONSTRAINT [FK__PersonSto__Statu__0AD2A005];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Feedback]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Feedback];
GO
IF OBJECT_ID(N'[dbo].[OrganizationContent]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationContent];
GO
IF OBJECT_ID(N'[dbo].[OrganizationEmployee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationEmployee];
GO
IF OBJECT_ID(N'[dbo].[OrganizationEvents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationEvents];
GO
IF OBJECT_ID(N'[dbo].[Organizations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organizations];
GO
IF OBJECT_ID(N'[dbo].[OrganizationSpecials]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationSpecials];
GO
IF OBJECT_ID(N'[dbo].[OrganizationStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationStatus];
GO
IF OBJECT_ID(N'[dbo].[OrganizationStocks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationStocks];
GO
IF OBJECT_ID(N'[dbo].[OrganizationStockTransactions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationStockTransactions];
GO
IF OBJECT_ID(N'[dbo].[Person]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Person];
GO
IF OBJECT_ID(N'[dbo].[PersonContent]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonContent];
GO
IF OBJECT_ID(N'[dbo].[personStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[personStatus];
GO
IF OBJECT_ID(N'[dbo].[PersonStocks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonStocks];
GO
IF OBJECT_ID(N'[dbo].[PersonStockTransactions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonStockTransactions];
GO
IF OBJECT_ID(N'[dbo].[Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Status];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Status'
CREATE TABLE [dbo].[Status] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NULL
);
GO

-- Creating table 'OrganizationEvents'
CREATE TABLE [dbo].[OrganizationEvents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrgID] int  NULL,
    [EventName] nvarchar(150)  NULL,
    [EventText] varchar(max)  NULL,
    [EventDate] datetime  NULL
);
GO

-- Creating table 'OrganizationSpecials'
CREATE TABLE [dbo].[OrganizationSpecials] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SpecialText] varchar(max)  NULL,
    [SpecialDate] datetime  NULL
);
GO

-- Creating table 'OrganizationStockTransactions'
CREATE TABLE [dbo].[OrganizationStockTransactions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrganizationID] int  NULL,
    [TransactionText] varchar(max)  NULL,
    [DateCreated] datetime  NULL
);
GO

-- Creating table 'PersonStockTransactions'
CREATE TABLE [dbo].[PersonStockTransactions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PersonID] int  NULL,
    [TransactionText] varchar(max)  NULL,
    [DateCreated] datetime  NULL
);
GO

-- Creating table 'OrganizationEmployees'
CREATE TABLE [dbo].[OrganizationEmployees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrgID] int  NULL,
    [PersonID] int  NULL
);
GO

-- Creating table 'OrganizationStocks'
CREATE TABLE [dbo].[OrganizationStocks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OwnerID] int  NULL,
    [OrgStockID] int  NULL,
    [NumStocks] int  NULL,
    [StatusID] int  NULL,
    [SellValue] decimal(12,2)  NULL,
    [isViewed] bit  NULL
);
GO

-- Creating table 'PersonStocks'
CREATE TABLE [dbo].[PersonStocks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OwnerID] int  NULL,
    [PersonStockID] int  NULL,
    [NumStocks] int  NULL,
    [StatusID] int  NULL,
    [SellValue] decimal(18,2)  NULL,
    [isViewed] bit  NULL
);
GO

-- Creating table 'Organizations'
CREATE TABLE [dbo].[Organizations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Email] nvarchar(75)  NULL,
    [Name] nvarchar(250)  NULL,
    [Passphrase] nvarchar(75)  NULL,
    [about] varchar(max)  NULL,
    [StockValue] decimal(18,2)  NULL,
    [HeaderImgLocation] nvarchar(200)  NULL,
    [HeaderText] nvarchar(200)  NULL
);
GO

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Email] nvarchar(75)  NULL,
    [FullName] nvarchar(75)  NULL,
    [Passphrase] nvarchar(250)  NULL,
    [StockValue] decimal(18,2)  NULL
);
GO

-- Creating table 'OrganizationContents'
CREATE TABLE [dbo].[OrganizationContents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(150)  NULL,
    [ItemPath] nvarchar(150)  NULL,
    [OrgID] int  NULL
);
GO

-- Creating table 'Feedbacks'
CREATE TABLE [dbo].[Feedbacks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(150)  NULL,
    [Feedback1] varchar(max)  NULL
);
GO

-- Creating table 'personStatus'
CREATE TABLE [dbo].[personStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PersonID] int  NULL,
    [Content] varchar(max)  NULL,
    [DateCreated] datetime  NULL
);
GO

-- Creating table 'OrganizationStatus'
CREATE TABLE [dbo].[OrganizationStatus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrgID] int  NULL,
    [Content] varchar(max)  NULL,
    [DateCreated] datetime  NULL
);
GO

-- Creating table 'PersonContents'
CREATE TABLE [dbo].[PersonContents] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(150)  NULL,
    [ItemPath] nvarchar(150)  NULL,
    [PersonID] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Status'
ALTER TABLE [dbo].[Status]
ADD CONSTRAINT [PK_Status]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationEvents'
ALTER TABLE [dbo].[OrganizationEvents]
ADD CONSTRAINT [PK_OrganizationEvents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationSpecials'
ALTER TABLE [dbo].[OrganizationSpecials]
ADD CONSTRAINT [PK_OrganizationSpecials]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationStockTransactions'
ALTER TABLE [dbo].[OrganizationStockTransactions]
ADD CONSTRAINT [PK_OrganizationStockTransactions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonStockTransactions'
ALTER TABLE [dbo].[PersonStockTransactions]
ADD CONSTRAINT [PK_PersonStockTransactions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationEmployees'
ALTER TABLE [dbo].[OrganizationEmployees]
ADD CONSTRAINT [PK_OrganizationEmployees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationStocks'
ALTER TABLE [dbo].[OrganizationStocks]
ADD CONSTRAINT [PK_OrganizationStocks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonStocks'
ALTER TABLE [dbo].[PersonStocks]
ADD CONSTRAINT [PK_PersonStocks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Organizations'
ALTER TABLE [dbo].[Organizations]
ADD CONSTRAINT [PK_Organizations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationContents'
ALTER TABLE [dbo].[OrganizationContents]
ADD CONSTRAINT [PK_OrganizationContents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Feedbacks'
ALTER TABLE [dbo].[Feedbacks]
ADD CONSTRAINT [PK_Feedbacks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'personStatus'
ALTER TABLE [dbo].[personStatus]
ADD CONSTRAINT [PK_personStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationStatus'
ALTER TABLE [dbo].[OrganizationStatus]
ADD CONSTRAINT [PK_OrganizationStatus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonContents'
ALTER TABLE [dbo].[PersonContents]
ADD CONSTRAINT [PK_PersonContents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [StatusID] in table 'OrganizationStocks'
ALTER TABLE [dbo].[OrganizationStocks]
ADD CONSTRAINT [FK__Organizat__Statu__1A14E395]
    FOREIGN KEY ([StatusID])
    REFERENCES [dbo].[Status]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__Statu__1A14E395'
CREATE INDEX [IX_FK__Organizat__Statu__1A14E395]
ON [dbo].[OrganizationStocks]
    ([StatusID]);
GO

-- Creating foreign key on [StatusID] in table 'PersonStocks'
ALTER TABLE [dbo].[PersonStocks]
ADD CONSTRAINT [FK__PersonSto__Statu__0AD2A005]
    FOREIGN KEY ([StatusID])
    REFERENCES [dbo].[Status]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PersonSto__Statu__0AD2A005'
CREATE INDEX [IX_FK__PersonSto__Statu__0AD2A005]
ON [dbo].[PersonStocks]
    ([StatusID]);
GO

-- Creating foreign key on [OrgID] in table 'OrganizationEmployees'
ALTER TABLE [dbo].[OrganizationEmployees]
ADD CONSTRAINT [FK__Organizat__OrgID__300424B4]
    FOREIGN KEY ([OrgID])
    REFERENCES [dbo].[Organizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__OrgID__300424B4'
CREATE INDEX [IX_FK__Organizat__OrgID__300424B4]
ON [dbo].[OrganizationEmployees]
    ([OrgID]);
GO

-- Creating foreign key on [PersonID] in table 'OrganizationEmployees'
ALTER TABLE [dbo].[OrganizationEmployees]
ADD CONSTRAINT [FK__Organizat__Perso__30F848ED]
    FOREIGN KEY ([PersonID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__Perso__30F848ED'
CREATE INDEX [IX_FK__Organizat__Perso__30F848ED]
ON [dbo].[OrganizationEmployees]
    ([PersonID]);
GO

-- Creating foreign key on [OrganizationID] in table 'OrganizationStockTransactions'
ALTER TABLE [dbo].[OrganizationStockTransactions]
ADD CONSTRAINT [FK__Organizat__Organ__2B3F6F97]
    FOREIGN KEY ([OrganizationID])
    REFERENCES [dbo].[Organizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__Organ__2B3F6F97'
CREATE INDEX [IX_FK__Organizat__Organ__2B3F6F97]
ON [dbo].[OrganizationStockTransactions]
    ([OrganizationID]);
GO

-- Creating foreign key on [OrgStockID] in table 'OrganizationStocks'
ALTER TABLE [dbo].[OrganizationStocks]
ADD CONSTRAINT [FK__Organizat__OrgSt__182C9B23]
    FOREIGN KEY ([OrgStockID])
    REFERENCES [dbo].[Organizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__OrgSt__182C9B23'
CREATE INDEX [IX_FK__Organizat__OrgSt__182C9B23]
ON [dbo].[OrganizationStocks]
    ([OrgStockID]);
GO

-- Creating foreign key on [OwnerID] in table 'OrganizationStocks'
ALTER TABLE [dbo].[OrganizationStocks]
ADD CONSTRAINT [FK__Organizat__Owner__1920BF5C]
    FOREIGN KEY ([OwnerID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__Owner__1920BF5C'
CREATE INDEX [IX_FK__Organizat__Owner__1920BF5C]
ON [dbo].[OrganizationStocks]
    ([OwnerID]);
GO

-- Creating foreign key on [OwnerID] in table 'PersonStocks'
ALTER TABLE [dbo].[PersonStocks]
ADD CONSTRAINT [FK__PersonSto__Owner__0519C6AF]
    FOREIGN KEY ([OwnerID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PersonSto__Owner__0519C6AF'
CREATE INDEX [IX_FK__PersonSto__Owner__0519C6AF]
ON [dbo].[PersonStocks]
    ([OwnerID]);
GO

-- Creating foreign key on [PersonStockID] in table 'PersonStocks'
ALTER TABLE [dbo].[PersonStocks]
ADD CONSTRAINT [FK__PersonSto__Perso__060DEAE8]
    FOREIGN KEY ([PersonStockID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PersonSto__Perso__060DEAE8'
CREATE INDEX [IX_FK__PersonSto__Perso__060DEAE8]
ON [dbo].[PersonStocks]
    ([PersonStockID]);
GO

-- Creating foreign key on [PersonID] in table 'PersonStockTransactions'
ALTER TABLE [dbo].[PersonStockTransactions]
ADD CONSTRAINT [FK__PersonSto__Perso__267ABA7A]
    FOREIGN KEY ([PersonID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PersonSto__Perso__267ABA7A'
CREATE INDEX [IX_FK__PersonSto__Perso__267ABA7A]
ON [dbo].[PersonStockTransactions]
    ([PersonID]);
GO

-- Creating foreign key on [OrgID] in table 'OrganizationContents'
ALTER TABLE [dbo].[OrganizationContents]
ADD CONSTRAINT [FK__Organizat__OrgID__398D8EEE]
    FOREIGN KEY ([OrgID])
    REFERENCES [dbo].[Organizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__OrgID__398D8EEE'
CREATE INDEX [IX_FK__Organizat__OrgID__398D8EEE]
ON [dbo].[OrganizationContents]
    ([OrgID]);
GO

-- Creating foreign key on [PersonID] in table 'personStatus'
ALTER TABLE [dbo].[personStatus]
ADD CONSTRAINT [FK__personCon__Perso__0F975522]
    FOREIGN KEY ([PersonID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__personCon__Perso__0F975522'
CREATE INDEX [IX_FK__personCon__Perso__0F975522]
ON [dbo].[personStatus]
    ([PersonID]);
GO

-- Creating foreign key on [OrgID] in table 'OrganizationStatus'
ALTER TABLE [dbo].[OrganizationStatus]
ADD CONSTRAINT [FK__Organizat__OrgID__46E78A0C]
    FOREIGN KEY ([OrgID])
    REFERENCES [dbo].[Organizations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Organizat__OrgID__46E78A0C'
CREATE INDEX [IX_FK__Organizat__OrgID__46E78A0C]
ON [dbo].[OrganizationStatus]
    ([OrgID]);
GO

-- Creating foreign key on [PersonID] in table 'PersonContents'
ALTER TABLE [dbo].[PersonContents]
ADD CONSTRAINT [FK__PersonCon__Perso__4222D4EF]
    FOREIGN KEY ([PersonID])
    REFERENCES [dbo].[People]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PersonCon__Perso__4222D4EF'
CREATE INDEX [IX_FK__PersonCon__Perso__4222D4EF]
ON [dbo].[PersonContents]
    ([PersonID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------