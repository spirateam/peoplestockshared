﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PeopleStock.Models;

namespace PeopleStock.Controllers
{
    public class OrganizationController : Controller
    {
        public ActionResult EditOrgAbout(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public ActionResult EditOrgAbout(int id, string About)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Id == id);
                org.about = About;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult AddOrgEvent(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public ActionResult AddOrgEvent(int id, string Name, string Event, string Date)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Id == id);
                DateTime eventDate = DateTime.Parse(Date);
                OrganizationEvent newEvent = new OrganizationEvent()
                {
                    EventDate = eventDate,
                    EventName = Name,
                    EventText = Event,
                    OrgID = id
                };
                db.OrganizationEvents.AddObject(newEvent);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult About(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var org = db.Organizations.FirstOrDefault(o => o.Id == id);
                return View(org);
            }
        }

        public ActionResult Personalize(string component)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Email == User.Identity.Name);
                string fileName = Path.GetFileName(org.HeaderImgLocation);
                string modelFilePath = string.Format(Url.Content("~/Resources/{0}/{1}"), org.Name, fileName);
                PersonalizeModel model = new PersonalizeModel()
                {
                    BannerImgLocation = modelFilePath ?? "/none",
                    BannerText = org.HeaderText
                }; 
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Personalize(HttpPostedFileBase file, string BannerText)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Email == User.Identity.Name);
                string modelFilePath = "none";
                if (file != null && !string.IsNullOrEmpty(file.FileName))
                {
                    modelFilePath = UploadFile(file, org, db);
                }
                if (!string.IsNullOrEmpty(BannerText))
                {
                    org.HeaderText = BannerText;
                    db.SaveChanges();
                }
                
                PersonalizeModel model = new PersonalizeModel()
                {
                    BannerImgLocation = modelFilePath,
                    BannerText = BannerText
                }; 
                return View(model);
            }
        }

        private string UploadFile(HttpPostedFileBase file, Organization org, PeopleStockEntities db)
        {
            //does this org have its own folder?
            string dir = string.Format(Url.Content("~/Resources/{0}"), org.Name);
            string serverDir = Server.MapPath(dir);
            if (!Directory.Exists(serverDir))
            {
                Directory.CreateDirectory(serverDir);
            }

            //save the file
            string filePath = Path.Combine(serverDir, file.FileName);
            if (!System.IO.File.Exists(filePath))
            {
                file.SaveAs(filePath);
            }
            if (org.HeaderImgLocation != filePath)
            {
                org.HeaderImgLocation = filePath;
                db.SaveChanges();
            }

            string fileName = Path.GetFileName(filePath);
            string modelFilePath = string.Format("/Resources/{0}/{1}", org.Name, fileName);
            return modelFilePath;
        }

        public ActionResult AddContent()
        {
            ViewBag.OrgName = User.Identity.Name;
            return View();
        }

        [HttpPost]
        public ActionResult AddContent(HttpPostedFileBase file_data, string Title)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Email == User.Identity.Name);
                HttpPostedFileBase file_upload = file_data;
                if (Utils.IsAudioResource(file_upload.FileName))
                {
                    UploadAudioResource(file_upload, org, db, Title);
                }
                else if (Utils.IsVideoResource(file_upload.FileName))
                {
                    UploadVideoResource(file_upload, org, db, Title);
                }
                ViewBag.OrgName = User.Identity.Name;
                return View();
            }
        }

        public string Upload(string folder, HttpPostedFileBase file_upload, string orgName, string title)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Email == orgName);
                string Title = title;
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[item] as HttpPostedFileBase;
                    if (Utils.IsAudioResource(hpf.FileName))
                    {
                        UploadAudioResource(hpf, org, db, Title);
                    }
                    else if (Utils.IsVideoResource(hpf.FileName))
                    {
                        UploadVideoResource(hpf, org, db, Title);
                    }
                }
            }
            return "Ok";
        }

        private string CreateResourceFolder(string orgName)
        {
            string dir = string.Format(Url.Content("~/Resources/{0}/Content"), orgName);
            string serverDir = Server.MapPath(dir);
            if (!Directory.Exists(serverDir))
            {
                Directory.CreateDirectory(serverDir);
            }
            return serverDir;
        }

        private void UploadAudioResource(HttpPostedFileBase file, Organization org, PeopleStockEntities db, string Title)
        {
            if (file == null) return;
            //does this org have its own folder?
            string serverDir = CreateResourceFolder(org.Name);

            //save the file
            string filePath = Path.Combine(serverDir, file.FileName);
            if (!System.IO.File.Exists(filePath))
            {
                file.SaveAs(filePath);
            }

            var existingContent = db.OrganizationContents.FirstOrDefault(o => o.Title == Title && o.OrgID == org.Id);
            if (existingContent == null)
            {
                OrganizationContent newContent = new OrganizationContent()
                {
                    ItemPath = file.FileName,
                    OrgID = org.Id,
                    Title = Title
                };
                db.OrganizationContents.AddObject(newContent);
                db.SaveChanges();
            }
        }

        private void UploadVideoResource(HttpPostedFileBase file, Organization org, PeopleStockEntities db, string Title)
        {
            //right now, we do nothing different to upload a video resource
            UploadAudioResource(file, org, db, Title);
        }

        public ActionResult RemoveContent(int orgID, int contentID)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var contentItem = db.OrganizationContents.FirstOrDefault(o => o.Id == contentID);
                if (contentItem != null)
                {
                    db.OrganizationContents.DeleteObject(contentItem);
                    db.SaveChanges();
                }
                return RedirectToAction("MainOrg", "Home");
            }
        }
    }
}
