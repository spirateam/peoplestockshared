﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using PeopleStock.Models;

namespace PeopleStock.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                using (PeopleStockEntities db = new PeopleStockEntities())
                {
                    var org = db.Organizations.FirstOrDefault(o => o.Email == User.Identity.Name);
                    //var person = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);
                    if (org != null)
                    {
                        return RedirectToAction("MainOrg");
                    }
                    return RedirectToAction("Main");
                }
            }
            return View();
        }

        public ActionResult Main()
        {
            try
            {
                using (PeopleStockEntities db = new PeopleStockEntities())
                {
                    Person currPerson = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);

                    //recalculate this person's stock value before we display it
                    
                    var ownedStocks = db.PersonStocks.Where(o => o.OwnerID == currPerson.Id);
                    decimal avgPersonStockValue = ownedStocks.Average(o => o.Person1.StockValue).Value;
                    decimal avgOrgStockValue = 0;
                    var oAvgOrgStockValue = db.OrganizationStocks.Where(o => o.OwnerID == currPerson.Id).Average(o => o.Organization.StockValue);
                    if (oAvgOrgStockValue.HasValue)
                    {
                        avgOrgStockValue = oAvgOrgStockValue.Value;
                    }
                    decimal avgStockValue = (avgPersonStockValue + avgOrgStockValue) / 2;
                    decimal newStockValue = avgStockValue;
                    decimal numStocks = db.PersonStocks.GroupBy(o => o.OwnerID).Count();
                    decimal stocksOwnedByOthers = db.PersonStocks.Where(o => o.PersonStockID == currPerson.Id).Count();
                    decimal popularityValue = (stocksOwnedByOthers / numStocks) * 10;
                    decimal popValue = popularityValue;
                    decimal popPercentage = (decimal)0.5;
                    decimal avgPercentage = (decimal)0.5;
                    decimal finalStockValue = ((avgPercentage * newStockValue) + (popPercentage * popValue));
                    if (finalStockValue < 1)
                    {
                        finalStockValue = 1;
                    }
                    if (currPerson.StockValue != finalStockValue)
                    {
                        currPerson.StockValue = finalStockValue;
                        db.SaveChanges();
                    }

                    currPerson.PersonStocks.ToList();
                    int sellStatus = Status.GetStatus(Constants.StockStatus.Sell,db);
                    foreach (var stock in currPerson.PersonStocks)
                    {
                        if (stock.Status == null)
                        {
                            stock.StatusID = sellStatus;
                            stock.SellValue = stock.Person1.StockValue;
                        }
                        string personName = stock.Person1.FullName;
                        string stockName = stock.Status.Name;
                    }
                    foreach (var orgStock in currPerson.OrganizationStocks)
                    {
                        if (orgStock.Status == null)
                        {
                            orgStock.StatusID = sellStatus;
                            orgStock.SellValue = orgStock.Organization.StockValue;
                        }
                        string orgName = orgStock.Organization.Name;
                    }
                    db.SaveChanges();

                    //get content in order
                    List<personStatu> contents = db.personStatus.Where(o => o.PersonID == currPerson.Id).OrderByDescending(o => o.DateCreated).ToList();
                    List<PersonStockTransaction> trans = db.PersonStockTransactions.Where(o => o.PersonID == currPerson.Id).OrderByDescending(o => o.DateCreated).ToList();

                    MainpageModel model = new MainpageModel()
                    {
                        CurrentPerson = currPerson,
                        Content = contents,
                        Transactions = trans
                    };
                    return View(model);
                }
            }
            catch
            {
                //try logging off the user
                FormsAuthentication.SignOut();
                return RedirectToAction("Index");
            }
        }

        public ActionResult MainOrg()
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization org = db.Organizations.FirstOrDefault(o => o.Email == User.Identity.Name);
                List<OrganizationEvent> orgEvents = db.OrganizationEvents.Where(o => o.OrgID == org.Id).ToList();
                string modelFilePath = Url.Content("~/Images/Beatles.jpg");
                if (!string.IsNullOrEmpty(org.HeaderImgLocation))
                {
                    string fileName = Path.GetFileName(org.HeaderImgLocation);
                    modelFilePath = string.Format(Url.Content("~/Resources/{0}/{1}"), org.Name, fileName);
                }

                MainOrgPageModel model = new MainOrgPageModel();
                model.Org = org;
                model.OrgEvents = orgEvents;
                model.BannerImgPath = modelFilePath;
                model.AudioContent = new List<OrganizationContent>();
                model.VideoContent = new List<OrganizationContent>();
                model.OrgStatus = db.OrganizationStatus.Where(o => o.OrgID == org.Id).ToList();
                var contentList = org.OrganizationContents.ToList();
                foreach (var content in contentList)
                {
                    if (Utils.IsAudioResource(content.ItemPath))
                    {
                        model.AudioContent.Add(content);
                    }
                    else if (Utils.IsVideoResource(content.ItemPath))
                    {
                        model.VideoContent.Add(content);
                    }
                }
                return View(model);
            }
        }

        public ActionResult ViewPersonalPage(int loggedInUserId, int viewUserId)
        {
            //get content in order
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                //mark this stock as viewed (for the viewer of course)
                var viewedStock = db.PersonStocks.FirstOrDefault(o => o.PersonStockID == viewUserId && o.OwnerID == loggedInUserId);
                if (!viewedStock.isViewed.Value)
                {
                    viewedStock.isViewed = true;
                    db.SaveChanges();
                }

                Person currPerson = db.People.FirstOrDefault(o => o.Id == viewUserId);
                List<personStatu> contents = db.personStatus.Where(o => o.PersonID == viewUserId).OrderByDescending(o => o.DateCreated).ToList();
                ViewBag.ContentList = contents;
                foreach (var stock in currPerson.PersonStocks)
                {
                    string sName = stock.NumStocks.ToString();
                    sName = stock.Person1.FullName;
                }
                return View(currPerson);
            }
        }

        public ActionResult ViewOrgPage(int loggedInUserId, int viewOrgId)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                //mark as viewed
                var viewedStock = db.OrganizationStocks.FirstOrDefault(o => o.OrgStockID == viewOrgId && o.OwnerID == loggedInUserId);
                var org = db.Organizations.FirstOrDefault(o => o.Id == viewOrgId);

                if (!viewedStock.isViewed.Value)
                {
                    viewedStock.isViewed = true;
                    db.SaveChanges();
                }
                string modelFilePath = Url.Content("~/Images/Beatles.jpg");
                if (!string.IsNullOrEmpty(org.HeaderImgLocation))
                {
                    string fileName = Path.GetFileName(org.HeaderImgLocation);
                    modelFilePath = string.Format(Url.Content("~/Resources/{0}/{1}"), org.Name, fileName);
                }

                List<OrganizationEvent> orgEvents = db.OrganizationEvents.Where(o => o.OrgID == org.Id).ToList();
                MainOrgPageModel model = new MainOrgPageModel();
                model.Org = org;
                model.OrgEvents = orgEvents;
                model.BannerImgPath = modelFilePath;
                model.AudioContent = new List<OrganizationContent>();
                model.VideoContent = new List<OrganizationContent>();
                model.OrgStatus = db.OrganizationStatus.Where(o => o.OrgID == org.Id).ToList();
                var contentList = org.OrganizationContents.ToList();
                foreach (var content in contentList)
                {
                    if (Utils.IsAudioResource(content.ItemPath))
                    {
                        model.AudioContent.Add(content);
                    }
                    else if (Utils.IsVideoResource(content.ItemPath))
                    {
                        model.VideoContent.Add(content);
                    }
                }
                return View(model);
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Feedback()
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var feedbackList = db.Feedbacks.ToList();
                return View(feedbackList);
            }
        }

        [HttpPost]
        public ActionResult Feedback(string Title, string Message)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Feedback newFeedback = new Feedback()
                {
                    Title = Title,
                    Feedback1 = Message
                };
                db.Feedbacks.AddObject(newFeedback);
                db.SaveChanges();

                var feedbackList = db.Feedbacks.ToList();
                return View(feedbackList);
            }
        }

        public ActionResult RemoveFeedback(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var feedback = db.Feedbacks.FirstOrDefault(o => o.Id == id);
                if (feedback != null)
                {
                    db.Feedbacks.DeleteObject(feedback);
                    db.SaveChanges();
                }
                return RedirectToAction("Feedback");
            }
        }
    }
}
