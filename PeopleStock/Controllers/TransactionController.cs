﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PeopleStock.Models;

namespace PeopleStock.Controllers
{
    public class TransactionController : Controller
    {
        //
        // GET: /Transaction/

        public ActionResult SearchStock()
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var stocks = db.PersonStocks.Include("Person").Where(o => o.Status.Name == Constants.StockStatus.Sell)
                    .OrderBy(o => o.Person1.FullName).ToList();
                var orgStocks = db.OrganizationStocks.Include("Person").Include("Organization").Where(o => o.Status.Name == Constants.StockStatus.Sell)
                    .OrderBy(o => o.Organization.Name).ToList();
                SearchStockModel model = new SearchStockModel();
                model.PersonStocks = stocks;
                model.OrgStocks = orgStocks;
                return View(model);
            }
        }

        public ActionResult BuyStock(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var stock = db.PersonStocks.Include("Person").FirstOrDefault(o => o.Id == id);
                var currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);
                var tradeStocks = db.PersonStocks.Where(o => o.OwnerID == currUser.Id);
                var orgTradeStocks = db.OrganizationStocks.Where(o => o.OwnerID == currUser.Id);
                string stockName = stock.Person1.FullName;
                BuyStockModel model = new BuyStockModel()
                {
                    PurchaseStock = stock,
                    TradeStocks = tradeStocks.ToList(),
                    OrgTradeStocks = orgTradeStocks.ToList()
                };
                foreach (var tradeStock in tradeStocks)
                {
                    string name = tradeStock.NumStocks.ToString();
                    name = tradeStock.Person1.FullName;
                }
                foreach (var orgStock in orgTradeStocks)
                {
                    string orgName = orgStock.Organization.Name;
                }

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult BuyStock(int PurchaseStockID, int PurchaseAmount, int[] stockQty, int[] stockID,int[] stockOrgQty, int[] stockOrgID)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                PersonStock stock = db.PersonStocks.FirstOrDefault(o => o.Id == PurchaseStockID);
                Person currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);
                Person ownerUser = db.People.FirstOrDefault(o => o.Id == stock.Person.Id);
                
                //trade person stocks from the initiator to the owner
                TradePersonStocks(stockID, stockQty, ownerUser, db);
                TradeOrgStocks(stockOrgQty, stockOrgID, ownerUser, db);

                //trade stock that the initiator purchased
                TradePersonStock(PurchaseStockID, PurchaseAmount, currUser, db);

                //log the transaction
                string logMessage = GetLogMessage(PurchaseStockID,PurchaseAmount,stockQty, stockID, stockOrgQty, stockOrgID, db);
                PersonStockTransaction trans = new PersonStockTransaction()
                {
                    PersonID = ownerUser.Id,
                    TransactionText = logMessage,
                    DateCreated = DateTime.Now
                };
                db.PersonStockTransactions.AddObject(trans);
                db.SaveChanges();

                return RedirectToAction("Main", "Home");
            }
        }

        private string GetLogMessage(int stockID, int qty, int[] stockQty, int[] stockIDs, int[] stockOrgQty, int[] stockOrgID, PeopleStockEntities db)
        {
            string msg = string.Empty;
            string subMsg = string.Empty;
            string msgTemplate = "{0} traded you {1} for {2} of {3}";
            string msgSubTemplate = "{0} of {1};";
            Person currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);

            PersonStock stock = db.PersonStocks.FirstOrDefault(o => o.Id == stockID);
            subMsg = GetLogMessage_Person(msgSubTemplate, stockQty, stockIDs, db);
            subMsg += GetLogMessage_Org(msgSubTemplate, stockOrgQty, stockOrgID, db);

            msg = string.Format(msgTemplate, currUser.FullName, subMsg, qty.ToString(), stock.Person1.FullName);

            return msg;
        }
        private string GetOrgLogMessage(int stockID, int qty, int[] stockQty, int[] stockIDs, int[] stockOrgQty, int[] stockOrgID, PeopleStockEntities db)
        {
            string msg = string.Empty;
            string subMsg = string.Empty;
            string msgTemplate = "{0} traded you {1} for {2} of {3}";
            string msgSubTemplate = "{0} of {1}";
            Person currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);

            OrganizationStock stock = db.OrganizationStocks.FirstOrDefault(o => o.Id == stockID);
            for (int i = 0; i < stockQty.Length; i++)
            {
                int currQty = stockQty[i];
                int currID = stockIDs[i];
                if (currQty <= 0) continue;
                PersonStock currStock = db.PersonStocks.FirstOrDefault(o => o.Id == currID);
                if (currStock == null) continue;
                subMsg += string.Format(msgSubTemplate, currQty.ToString(), currStock.Person1.FullName);
            }
            for (int i = 0; i < stockOrgQty.Length; i++)
            {
                int currQty = stockOrgQty[i];
                int currID = stockOrgID[i];
                if (currQty <= 0) continue;
                OrganizationStock currStock = db.OrganizationStocks.FirstOrDefault(o => o.Id == currID);
                if (currStock == null) continue;
                subMsg += string.Format(msgSubTemplate, currQty.ToString(), currStock.Organization.Name);
            }

            msg = string.Format(msgTemplate, currUser.FullName, subMsg, qty.ToString(), stock.Organization.Name);

            return msg;
        }
        private string GetLogMessage_Person(string msgSubTemplate, int[] stockQty, int[] stockIDs, PeopleStockEntities db)
        {
            if (stockQty == null) return string.Empty;
            string msg = string.Empty;
            for (int i = 0; i < stockQty.Length; i++)
            {
                int currQty = stockQty[i];
                int currID = stockIDs[i];
                if (currQty <= 0) continue;
                PersonStock currStock = db.PersonStocks.FirstOrDefault(o => o.Id == currID);
                if (currStock == null) continue;
                msg += string.Format(msgSubTemplate, currQty.ToString(), currStock.Person1.FullName);
            }
            return msg;
        }
        private string GetLogMessage_Org(string msgSubTemplate,int[] stockOrgQty, int[] stockOrgID, PeopleStockEntities db)
        {
            if (stockOrgQty == null) return string.Empty;
            string msg = string.Empty;
            for (int i = 0; i < stockOrgQty.Length; i++)
            {
                int currQty = stockOrgQty[i];
                int currID = stockOrgID[i];
                if (currQty <= 0) continue;
                OrganizationStock currStock = db.OrganizationStocks.FirstOrDefault(o => o.Id == currID);
                if (currStock == null) continue;
                msg += string.Format(msgSubTemplate, currQty.ToString(), currStock.Organization.Name);
            }
            return msg;
        }

        public ActionResult StockSettings(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                PersonStock stock = db.PersonStocks.FirstOrDefault(o => o.Id == id);
                return View(stock);
            }
        }

        [HttpPost]
        public ActionResult StockSettings(int id, string rdoSell, string SellValue)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                PersonStock stock = db.PersonStocks.FirstOrDefault(o => o.Id == id);
                if (rdoSell == "Sell")
                {
                    decimal dSellValue = decimal.Parse(SellValue);
                    Status sellStatus = db.Status.FirstOrDefault(o => o.Name == Constants.StockStatus.Sell);
                    stock.StatusID = sellStatus.Id;
                    stock.SellValue = dSellValue;
                }
                else
                {
                    Status holdStatus = db.Status.FirstOrDefault(o => o.Name == Constants.StockStatus.Hold);
                    stock.StatusID = holdStatus.Id;
                }
                db.SaveChanges();
                return RedirectToAction("Main", "Home");
            }
        }

        public ActionResult OrgStockSettings(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                OrganizationStock stock = db.OrganizationStocks.FirstOrDefault(o => o.Id == id);
                return View(stock);
            }
        }

        [HttpPost]
        public ActionResult OrgStockSettings(int id, string rdoSell, string SellValue)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                OrganizationStock stock = db.OrganizationStocks.FirstOrDefault(o => o.Id == id);
                if (rdoSell == "Sell")
                {
                    decimal dSellValue = decimal.Parse(SellValue);
                    Status sellStatus = db.Status.FirstOrDefault(o => o.Name == Constants.StockStatus.Sell);
                    stock.StatusID = sellStatus.Id;
                    stock.SellValue = dSellValue;
                }
                else
                {
                    Status holdStatus = db.Status.FirstOrDefault(o => o.Name == Constants.StockStatus.Hold);
                    stock.StatusID = holdStatus.Id;
                }
                db.SaveChanges();
                return RedirectToAction("Main", "Home");
            }
        }

        public ActionResult BuyOrgStock(int id)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                var stock = db.OrganizationStocks.Include("Person").Include("Organization").FirstOrDefault(o => o.Id == id);
                var currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);
                var tradeStocks = db.PersonStocks.Where(o => o.OwnerID == currUser.Id);
                var tradeOrgStocks = db.OrganizationStocks.Where(o => o.OwnerID == currUser.Id);
                BuyOrgStockModel model = new BuyOrgStockModel()
                {
                    PurchaseStock = stock,
                    TradeStocks = tradeStocks.ToList(),
                    TradeOrgStocks = tradeOrgStocks.ToList()
                };
                foreach (var tradeStock in tradeStocks)
                {
                    string name = tradeStock.NumStocks.ToString();
                    name = tradeStock.Person1.FullName;
                }
                foreach (var orgStock in tradeOrgStocks)
                {
                    string name = orgStock.Organization.Name;
                    name = orgStock.Person.FullName;
                }
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult BuyOrgStock(int PurchaseStockID, int PurchaseAmount, int[] stockQty, int[] stockID, int[] stockOrgQty, int[] stockOrgID)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                OrganizationStock stock = db.OrganizationStocks.FirstOrDefault(o => o.Id == PurchaseStockID);
                Person currUser = db.People.FirstOrDefault(o => o.Email == User.Identity.Name);
                Person ownerUser = db.People.FirstOrDefault(o => o.Id == stock.Person.Id);
                //trade out person stocks from the trade initiator to the stock owner
                TradePersonStocks(stockID, stockQty, ownerUser, db);

                //trade out org stocks from the trade initiator to the stock owner
                TradeOrgStocks(stockOrgQty, stockOrgID, ownerUser, db);

                //trade the actual org stocks to the trade initiator 
                TradeOrgStock(PurchaseStockID, PurchaseAmount, currUser, db);

                db.SaveChanges();

                return RedirectToAction("Main", "Home");
            }
        }

        /// <summary>
        /// trades a group of stocks from the logged in user, to the specified user
        /// </summary>
        /// <param name="stocks"></param>
        /// <param name="stockQtys"></param>
        private void TradePersonStocks(int[] stocks, int[] stockQtys, Person newUser, PeopleStockEntities db)
        {
            if (stocks == null || stocks.Length <= 0) return;
            for (int i = 0; i < stocks.Length; i++)
            {
                int stockID = stocks[i];
                int stockQty = stockQtys[i];
                if (stockQty <= 0) continue;
                TradePersonStock(stockID, stockQty, newUser, db);
            }
        }

        /// <summary>
        /// Trade the stock from the logged-in user, to the specified user
        /// </summary>
        /// <param name="stockID"></param>
        /// <param name="stockQty"></param>
        /// <param name="db"></param>
        private void TradePersonStock(int stockID, int stockQty, Person newUser, PeopleStockEntities db)
        {
            //get the current stock, we need some values from it
            PersonStock currStock = db.PersonStocks.FirstOrDefault(o => o.Id == stockID);
            int holdStatus = Status.GetStatus(Constants.StockStatus.Hold, db);

            //remove the stock from the current owner before we give to the new owner
            currStock.NumStocks -= stockQty;

            //does the new owner already have some of this stock?
            var ownedStock = db.PersonStocks.FirstOrDefault(o => o.PersonStockID == currStock.PersonStockID && o.OwnerID == newUser.Id);
            if (ownedStock != null)
            {
                ownedStock.NumStocks += stockQty;
                db.SaveChanges();
                return;
            }

            //new owner doesn't have this stock, create new stock and give it to them
            PersonStock newStock = new PersonStock()
            {
                NumStocks = stockQty,
                OwnerID = newUser.Id,
                SellValue = currStock.SellValue,
                StatusID = holdStatus,
                PersonStockID = currStock.PersonStockID,
                isViewed = false
            };
            db.PersonStocks.AddObject(newStock);
            db.SaveChanges();
        }

        /// <summary>
        /// Trades Organization stocks from the current stock owner, to the specified user
        /// </summary>
        /// <param name="stocks"></param>
        /// <param name="stockQtys"></param>
        /// <param name="newUser"></param>
        /// <param name="db"></param>
        private void TradeOrgStocks(int[] stockQtys, int[] stocks, Person newUser, PeopleStockEntities db)
        {
            if (stocks == null || stocks.Length <= 0) return;
            for (int i = 0; i < stocks.Length; i++)
            {
                int stockID = stocks[i];
                int stockQty = stockQtys[i];
                if (stockQty <= 0) continue;
                TradeOrgStock(stockID, stockQty, newUser, db);
            }
        }

        /// <summary>
        /// Trades an organization stock from the current owner to the specified user
        /// </summary>
        /// <param name="stockID"></param>
        /// <param name="stockQty"></param>
        /// <param name="newUser"></param>
        /// <param name="db"></param>
        private void TradeOrgStock(int stockID, int stockQty, Person newUser, PeopleStockEntities db)
        {
            //get the current stock, we need some values from it
            OrganizationStock currStock = db.OrganizationStocks.FirstOrDefault(o => o.Id == stockID);
            int holdStatus = Status.GetStatus(Constants.StockStatus.Hold, db);

            //remove the stock from the current owner before we give to the new owner
            currStock.NumStocks -= stockQty;

            //does the new owner already have some of this stock?
            var ownedStock = db.OrganizationStocks.FirstOrDefault(o => o.OrgStockID == currStock.OrgStockID && o.OwnerID == newUser.Id);
            if (ownedStock != null)
            {
                ownedStock.NumStocks += stockQty;
                db.SaveChanges();
                return;
            }

            OrganizationStock newStock = new OrganizationStock()
            {
                NumStocks = stockQty,
                OwnerID = newUser.Id,
                SellValue = currStock.SellValue,
                StatusID = holdStatus,
                OrgStockID = currStock.OrgStockID,
                isViewed = false
            };
            db.OrganizationStocks.AddObject(newStock);
            db.SaveChanges();
        }
    }
}
