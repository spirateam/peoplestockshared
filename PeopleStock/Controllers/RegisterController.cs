﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using PeopleStock.Models;

namespace PeopleStock.Controllers
{
    public class RegisterController : Controller
    {
        public ActionResult RegisterPerson()
        {
            return View();
        }

        public ActionResult RegisterOrg()
        {
            Organization org = new Organization();
            return View(org);
        }

        [HttpPost]
        public ActionResult RegisterOrg(string Name, string Email, string Passphrase)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                Organization newOrg = new Organization()
                {
                    Email = Email,
                    Name = Name,
                    Passphrase = Passphrase,
                    StockValue = Constants.StartingOrgStockValue
                };
                var currOrg = db.Organizations.FirstOrDefault(o => o.Name == Name);
                if (currOrg != null)
                {
                    string errorMessage = "Organization already exists.";
                    return RedirectToAction("Error", "Error", new {ErrorMessage = errorMessage });
                }
                db.Organizations.AddObject(newOrg);
                db.SaveChanges();

                //create a default user to represent this org
                Person orgPerson = new Person()
                {
                    Email = newOrg.Email,
                    FullName = newOrg.Name,
                    Passphrase = newOrg.Passphrase
                };
                db.People.AddObject(orgPerson);
                db.SaveChanges();

                int sellStatus = Status.GetStatus(Constants.StockStatus.Sell);

                //create stocks for the new org
                OrganizationStock newStock = new OrganizationStock()
                {
                    NumStocks = Constants.StartingOrgStockCount,
                    OrgStockID = newOrg.Id,
                    OwnerID = orgPerson.Id,
                    SellValue = 1,
                    StatusID = sellStatus,
                };
                db.OrganizationStocks.AddObject(newStock);
                db.SaveChanges();

                FormsAuthentication.SetAuthCookie(newOrg.Email, false);
                return RedirectToAction("MainOrg", "Home");
            }
        }

    }
}
