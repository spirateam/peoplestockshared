﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeopleStock
{
    public class Constants
    {
        public const string Version = "1.0.0.1";
        
        public static readonly int StartingStockCount = 1000;
        public static readonly int StartingStockValue = 1;
        public static readonly int StartingOrgStockCount = 1000;
        public static readonly int StartingOrgStockValue = 1;
        public static readonly int MaxStockValue = 10;

        public class StockStatus
        {
            public static readonly string Sell = "Sell";
            public static readonly string Hold = "Hold";
        }

        public class OrganizationComponents
        {
            public static readonly string Banner = "Banner";


        }
    }
}