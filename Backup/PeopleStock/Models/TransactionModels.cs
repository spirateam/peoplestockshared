﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeopleStock.Models
{
    public class BuyStockModel
    {
        public PersonStock PurchaseStock { get; set; }
        public List<PersonStock> TradeStocks { get; set; }
        public List<OrganizationStock> OrgTradeStocks { get; set; }
    }

    public class BuyOrgStockModel
    {
        public OrganizationStock PurchaseStock { get; set; }
        public List<OrganizationStock> TradeOrgStocks { get; set; }
        public List<PersonStock> TradeStocks { get; set; }
    }

    public class SearchStockModel
    {
        public List<PersonStock> PersonStocks { get; set; }
        public List<OrganizationStock> OrgStocks { get; set; }
    }
}