﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeopleStock.Models
{
    public partial class Status
    {
        public static int GetStatus(string status)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                return GetStatus(status, db);
            }
        }
        public static int GetStatus(string status,PeopleStockEntities db)
        {
            Status stat = db.Status.FirstOrDefault(o => o.Name == status);
            return stat.Id;
        }
    }
}