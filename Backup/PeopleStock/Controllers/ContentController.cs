﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PeopleStock.Models;

namespace PeopleStock.Controllers
{
    public class ContentController : Controller
    {
        //
        // GET: /Content/

        public ActionResult AddContent(int id)
        {
            ViewBag.PersonID = id;
            return View();
        }

        [HttpPost]
        public ActionResult AddContent(int PersonID, string Content)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                DateTime currTime = DateTime.Now;
                personStatu newContent = new personStatu()
                {
                    Content = Content,
                    DateCreated = currTime,
                    PersonID = PersonID
                };
                db.personStatus.AddObject(newContent);
                db.SaveChanges();
                return RedirectToAction("Main", "Home");
            }
        }

        public ActionResult AddOrgContent(int id)
        {
            OrganizationStatu newStatus = new OrganizationStatu();
            newStatus.OrgID = id;
            return View(newStatus);
        }

        [HttpPost]
        public ActionResult AddOrgContent(int id, OrganizationStatu newStatus)
        {
            using (PeopleStockEntities db = new PeopleStockEntities())
            {
                newStatus.DateCreated = DateTime.Now;
                newStatus.OrgID = id;
                newStatus.Id = -1;
                db.OrganizationStatus.AddObject(newStatus);
                db.SaveChanges();
                return RedirectToAction("MainOrg", "Home");
            }
        }
    }
}
